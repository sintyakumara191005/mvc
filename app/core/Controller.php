<?php 

class Controller {

    public function model($model) 
    {
        require_once("../models/{$model}.php");
        return new $model ;
    }

    public function view($view, $params = []) 
    {
        require_once("../view/{$view}.php");
    }
}

?>
